/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1234;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Label Lbprint;
    @FXML
    private Button but1;
    @FXML
    private Button btnCal;
    @FXML
    private Button but2;
    @FXML
    private Button but3;
    @FXML
    private Button but4;
    @FXML
    private Button but5;
    @FXML
    private Button but6;
    @FXML
    private Button but7;
    @FXML
    private Button but8;
    @FXML
    private Button but9;
    @FXML
    private Button but0;
     @FXML
    private Button but10;
      @FXML
    private Button but11;
    @FXML
     private Button but12;
    @FXML
     private Button but13;
     @FXML
     private Button but14;
      @FXML
     private Button but15;
        @FXML
     private Button but16;
    @FXML
    private Button butplus;
    @FXML
    private Button butsub;
    @FXML
    private Button butmul;
    @FXML
    private Button butdiv;
    private String op;
    private double num,num1,ans;
    @FXML
    private Button butpoint;
    @FXML
    private Button butC;
    @FXML
    private Button butAC;
    @FXML
    private Button but17;
    @FXML
    private Button but18;
    @FXML
    private Button but19;
   
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleButtonNum(ActionEvent event) {
        String value=((Button)event.getSource()).getText();
        if(!Lbprint.getText().isEmpty())
        {
            value=Lbprint.getText().concat(value);
        }
       Lbprint.setText(value);   
       
    }

    @FXML
    private void handleButtonCal(ActionEvent event) { 
        
        if(!Lbprint.getText().isEmpty())
        {
        switch(op)
        {
            case "+":
               num1=Double.parseDouble(Lbprint.getText());
               ans=num+num1;
               Lbprint.setText(String.valueOf(ans));
               break;
             case "-":
               num1=Double.parseDouble(Lbprint.getText());
               ans=num-num1;
               Lbprint.setText(String.valueOf(ans));
               break;
             case "*":
               num1=Double.parseDouble(Lbprint.getText());
               ans=num*num1;
               Lbprint.setText(String.valueOf(ans));
               break;
             case "/":
               num1=Double.parseDouble(Lbprint.getText());
               ans=num/num1;
               Lbprint.setText(String.valueOf(ans));
               break;
                case "x^y":
               num1=Double.parseDouble(Lbprint.getText());
               Math.pow(num,num1);
               ans=Math.pow(num,num1);
               Lbprint.setText(String.valueOf(ans));
               break;   
              
              
        }
       
        }
        
    }

    @FXML
    private void handleButtonOp(ActionEvent event) {
         String operation=((Button)event.getSource()).getText();
         op=operation;
         num=Double.parseDouble(Lbprint.getText());
         Lbprint.setText("");
         
    }

    @FXML
    private void handleButtonC(ActionEvent event) {
        Lbprint.setText("");
    }

    @FXML
    private void handleButtonAC(ActionEvent event) {
         op=null;
         Lbprint.setText("");
    }

    @FXML
    private void handleButtona(ActionEvent event) {
        
        
  num1=Double.parseDouble(Lbprint.getText());
               ans=num1/100;
               Lbprint.setText(String.valueOf(ans));
        
    }

    @FXML
    private void handleButtonb(ActionEvent event) {
        num1=Double.parseDouble(Lbprint.getText());
               Math.sqrt(num1);
               ans=Math.sqrt(num1);
               Lbprint.setText(String.valueOf(ans));
              
    }

    @FXML
    private void handleButtonc(ActionEvent event) {
        num1=Double.parseDouble(Lbprint.getText());
               Math.sin(num1);
               ans=Math.sin(num1/180.0*Math.PI);
               Lbprint.setText(String.valueOf(ans));
    }

    @FXML
    private void handleButtond(ActionEvent event) {
        num1=Double.parseDouble(Lbprint.getText());
               Math.cos(num1);
               ans=Math.cos(num1/180.0*Math.PI);
               Lbprint.setText(String.valueOf(ans));
    }

    @FXML
    private void handleButtonf(ActionEvent event) {
         num1=Double.parseDouble(Lbprint.getText());
         num1 = Math.toRadians(num1);
               Math.tan(num1);
               ans=Math.tan(num1);
               Lbprint.setText(String.valueOf(ans));
    }

    @FXML
    private void handleButtong(ActionEvent event) {
        num1=Double.parseDouble(Lbprint.getText());
               Math.abs(num1);
               ans=Math.abs(num1);
               Lbprint.setText(String.valueOf(ans));
    }

    @FXML
    private void handleButtonh(ActionEvent event) {
      Lbprint.setText(String.valueOf(3.14));
    }

    @FXML
    private void handleButtoni(ActionEvent event) {
         Lbprint.setText(String.valueOf("-"));
    }


    
  
   
}
